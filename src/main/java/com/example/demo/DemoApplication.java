package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;
import io.sentry.Sentry;

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
		String sentryDsn = System.getenv("SENTRY_DSN");
		Sentry.init(sentryDsn);

		Sentry.capture("This is a test, now checked in");

		return "Spring is here!";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}